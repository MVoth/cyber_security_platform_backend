Cyber Security Challenge Platform Backend
=========================================

# Overview

This project contains the frontend for the FH Campus Wien Cyber Security Challenge Platform

# Contribution

Development follows the [git branching model](http://nvie.com/posts/a-successful-git-branching-model/) unsing the standard prefixes of [git flow](https://github.com/nvie/gitflow).

The format of the Changelog is based on [Keep a Changelog](http://keepachangelog.com/) and this project adheres to [Semantic Versioning](http://semver.org/).

# Usage

## Requirements

The software has been tested under Ubuntu 16.04 Xenial, 64 bit.

You need to install the following packages: `mono-complete mono-xsp4`

## Running a test server

1. Clone the repository
2. Build code with `xbuild src/Hackathon.sln`
3. Change into the Service directory: `cd src/Hackathon`
4. Start the test server: `xsp4 --applications=/:. --address=127.0.0.1 --port=8999 --nonstop`
5 Der server is connecting to a local MySQL Server on port 3306. The database name and credentials can be configured in the file `web.config`. The database and user have to exists, tables are created on the first run automatically.
7. The password hashes in the User table are stored in the format `BASE64(SHA_256(nickname|password))`



