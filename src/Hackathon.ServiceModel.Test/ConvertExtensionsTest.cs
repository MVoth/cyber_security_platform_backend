﻿//
//  ConvertExtensionsTest.cs
//
//  Author:
//       Mathias Tausig <mathias.tausig@fh-campuswien.ac.at>
//
//  Copyright (c) 2016 University of Applied Sciences Campus Vienna
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

using NUnit.Framework;
using System;

namespace Hackathon.ServiceModel.Test
{
	[TestFixture ()]
	public class ConvertExtensionsTest
	{
		[Test ()]
		public void ToEcmaScriptStringTest ()
		{
			DateTime date = new DateTime (2016, 12, 14, 13, 59, 0, 314, DateTimeKind.Utc);
			Assert.That (date.ToEcmaScriptString (), Is.EqualTo ("2016-12-14T13:59:00.314Z"));
		}

		[Test ()]
		public void FromEcmaScriptStringTest ()
		{
			string dateString = "2016-12-14T13:59:00.314Z";
			Assert.That (dateString.FromEcmaScriptString (), Is.EqualTo (new DateTime (2016, 12, 14, 13, 59, 0, 314, DateTimeKind.Utc)));
		}

		[Test ()]
		public void FromEcmaScriptStringTimeZoneTest ()
		{
			string dateString = "2016-12-14T13:59:00.314+01:00";
			Assert.That (dateString.FromEcmaScriptString (), Is.EqualTo (new DateTime (2016, 12, 14, 12, 59, 0, 314, DateTimeKind.Utc)));
		}
	}
}

