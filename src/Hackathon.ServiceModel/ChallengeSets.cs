﻿//
//  ChallengeSets.cs
//
//  Author:
//       Mathias Tausig <mathias.tausig@fh-campuswien.ac.at>
//
//  Copyright (c) 2016 University of Applied Sciences Campus Vienna
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using ServiceStack;
using System.Collections.Generic;

namespace Hackathon.ServiceModel
{
	[Route ("/challengesets/", "GET")]
	public class FindChallengeSets : IReturn<IList<int>>
	{
		public int? ChallengeId { get; set; }
	}

	[Route ("/challengesets/{id}", "GET")]
	public class GetChallengeSet : IReturn<GetChallengeSetResponse>
	{
		public int Id { get; set; }
	}

	public class GetChallengeSetResponse
	{
		public int Id { get; set; }

		public string Title { get; set; }

		public string Description { get; set; }

		public List<int> Challenges { get; set; }
	}
}

