﻿//
//  Global.asax.cs
//
//  Author:
//       Mathias Tausig <mathias.tausig@fh-campuswien.ac.at>
//
//  Copyright (c) 2016 University of Applied Sciences Campus Vienna
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections;
using System.Web;
using System.Web.SessionState;
using ServiceStack;
using Funq;
using ServiceStack.Validation;
using ServiceStack.Data;
using ServiceStack.OrmLite;
using ServiceStack.MiniProfiler.Data;
using ServiceStack.MiniProfiler;
using Hackathon.ServiceInterface;
using Hackathon.ServiceModel.Types;
using ServiceStack.Auth;
using ServiceStack.Caching;
using Hackathon.ServiceModel.Auth;
using Hackathon.ServiceInterface.Notification;
using System.Collections.Generic;
using Hackathon.ServiceInterface.Points;
using Hackathon.ServiceModel.Validators;
using System.Security.Cryptography;


namespace Hackathon
{
	public class AppHost : AppHostBase
	{
		public AppHost () : base ("Hackathon Database Service",	typeof(UsersService).Assembly)
		{
		}

		public override void Configure (Container container)
		{
			Plugins.Add (new ValidationFeature ());
			// Required for testing with frontend and backend on different locations, because of the same origin policy
			Plugins.Add (new CorsFeature (allowOriginWhitelist: new[]{ "http://localhost:63342" }, allowCredentials: true));
			//container.RegisterValidators(typeof(ContactsServices).Assembly);

			Plugins.Add (new ValidationFeature ());
			//This method scans the assembly for validators
			container.RegisterValidators (typeof(ChangeUserValidator).Assembly);

			string mysqlConnectionString = System.Web.Configuration.WebConfigurationManager.AppSettings ["MysqlConnectionString"].ToString ();
			var connectionFactory = new OrmLiteConnectionFactory (mysqlConnectionString, ServiceStack.OrmLite.MySql.MySqlDialectProvider.Instance);
			container.Register<IDbConnectionFactory> (connectionFactory);

			container.RegisterAs<DbPoints, IPoints> ();

			byte[] userTokenKey = Convert.FromBase64String (System.Web.Configuration.WebConfigurationManager.AppSettings ["UserTokenKey"].ToString ());
			HMACSHA256 userTokenCalculator = new HMACSHA256 (userTokenKey);
			container.Register<HMAC> (userTokenCalculator);

			using (var db = container.Resolve<IDbConnectionFactory> ().Open ()) {

				// Uncomment the following block, in order tho completely reset the database

//				db.DropTable<Attachment> ();
//				db.DropTable<HintAccess> ();
//				db.DropTable<EventUsers> ();
//				db.DropTable<EventChallengeSets> ();
//				db.DropTable<Event> ();
//				db.DropTable<ChallengeSolution> ();
//				db.DropTable<ChallengeSet> ();
//				db.DropTable<ChallengeAccess> ();
//				db.DropTable<Hint> ();
//				db.DropTable<Challenge> ();
//				db.DropTable<User> ();

				db.CreateTableIfNotExists<Attachment> ();
				db.CreateTableIfNotExists<User> ();
				db.CreateTableIfNotExists<Challenge> ();
				db.CreateTableIfNotExists<Hint> ();
				db.CreateTableIfNotExists<ChallengeAccess> ();
				db.CreateTableIfNotExists<ChallengeSet> ();
				db.CreateTableIfNotExists<ChallengeSolution> ();
				db.CreateTableIfNotExists<Event> ();
				db.CreateTableIfNotExists<EventChallengeSets> ();
				db.CreateTableIfNotExists<EventUsers> ();
				db.CreateTableIfNotExists<HintAccess> ();

				// Uncomment the following block, in order to initialise the database with some dummy data

//				db.Insert (new User {
//					UserName = "foo",
//					FirstName = "Foo",
//					LastName = "bar",
//					Avatar = null,
//					MailAddress = "foobar@mailinator.com",
//					Salt = "foobar",
//					//pw=123456, created with echo -n "foobar123456"|sha256sum, then base64 encoded
//					Password = @"V7yttgjbKKO8rKp6OTiHqQ8BCSEKqjaGgBtun4lWjfs=",
//					PasswordChangeDate = new DateTime (2016, 9, 7, 16, 56, 0),
//				});
//
//				db.Insert (new Challenge {
//					Id = 1,
//					Title = "foobar",
//					Description = "Foo bar foo bar.",
//					BasePoints = 100,
//				});
//
//				db.Insert (new Hint {
//					Id = 1,
//					ChallengeId = 1,
//					Text = "This might help you.",
//					PointReduction = 0,
//				});
//
//				db.Insert (new ChallengeSet {
//					Id = 1,
//					Title = "foobar",
//					Description = "Foo bar foo bar.",
//					Challenges = new List<int> () { 1, 2, 42 },
//				});
//
//				db.Insert (new ChallengeSet {
//					Id = 2,
//					Title = "bla",
//					Description = "Bla bla bla.",
//					Challenges = new List<int> () { 3 },
//				});
//
//				db.Insert (new Event {
//					Id = 1,
//					Title = "foobar",
//					Description = "Foo bar foo bar.",
//				});
//
//				db.Insert (new Event {
//					Id = 2,
//					Title = "Empty event",
//					Description = "Nothing to be done.",
//				});
//
//				db.Insert (new EventChallengeSets {
//					EventId = 1,
//					ChallengeSetId = 1,
//					StartDate = DateTime.Now,
//					EndDate = DateTime.Now.AddDays (10),
//				});
//
//				db.Insert (new EventChallengeSets {
//					EventId = 1,
//					ChallengeSetId = 2,
//					StartDate = DateTime.Now,
//					EndDate = DateTime.Now.AddDays (10),
//				});
			}

			Plugins.Add (new AuthFeature (
				() => new AuthUserSession (),
				new IAuthProvider[] { new DbAuthProvider () }) {
				// Do not redirect when trying to access a service with an AuthenticatedAttribute (only valid for content type HTML), just return the error code instead
				HtmlRedirect = null,
			}
			);

			container.Register<ICacheClient> (new MemoryCacheClient ());
			var userRep = new InMemoryAuthRepository ();
			container.Register<IUserAuthRepository> (userRep);
			// Register the default way to notify a user
			container.Register<INotifyUser> (new SendEmail ());
		}
	}

	public class Global : System.Web.HttpApplication
	{
		protected void Application_Start (object sender, EventArgs e)
		{
			new AppHost ().Init ();
		}

		protected void Application_BeginRequest (object src, EventArgs e)
		{
//			if (Request.IsLocal)
//				Profiler.Start ();
		}

		protected void Application_EndRequest (object src, EventArgs e)
		{
//			Profiler.Stop ();
		}
	}
}
