﻿//
//  ChallengeSetsServiceTest.cs
//
//  Author:
//       Mathias Tausig <mathias.tausig@fh-campuswien.ac.at>
//
//  Copyright (c) 2016 University of Applied Sciences Campus Vienna
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Linq;
using NUnit.Framework;
using ServiceStack.Testing;
using ServiceStack.Data;
using ServiceStack.OrmLite;
using ServiceStack;
using ServiceStack.Auth;
using Hackathon.ServiceModel.Types;
using System.Collections.Generic;
using Hackathon.ServiceModel;

namespace Hackathon.ServiceInterface.Test
{
	[TestFixture ()]
	public class ChallengeSetsServiceTest
	{
		private class TestAppHost : BasicAppHost
		{
			public TestAppHost () : base (typeof(ChallengeSetsService).Assembly)
			{
			}

			public override void Configure (Funq.Container container)
			{
				container.Register<IDbConnectionFactory> (c =>
					new OrmLiteConnectionFactory (":memory:", SqliteDialect.Provider)
				);

				container.Register<IAuthSession> (c => new AuthUserSession {
					UserAuthName = "foobar@mailinator.com",
					UserName = "foobar",
					UserAuthId = "1",
				});

				//container.RegisterAs<DbEmailer, IEmailer>();

				using (var db = container.TryResolve<IDbConnectionFactory> ().Open ()) {
					db.DropAndCreateTable<User> ();
					db.DropAndCreateTable<ChallengeSet> ();

					db.Insert (new ChallengeSet {
						Id = 1,
						Title = "foobar",
						Description = "Foo bar foo bar.",
						Challenges = new List<int> () { 1, 2, 42 },
					});

					db.Insert (new ChallengeSet {
						Id = 2,
						Challenges = new List<int> () { 42, 43 },
					});

					db.Insert (new User {
						Id = 1,
						UserName = "foobar",
						FirstName = "Foo",
						LastName = "bar",
						Avatar = null,
						MailAddress = "foobar@mailinator.com",
						Salt = "foobar",
						//pw=123456, created with echo -n "foobar123456"|sha256sum, then base64 encoded
						Password = @"V7yttgjbKKO8rKp6OTiHqQ8BCSEKqjaGgBtun4lWjfs=",
					});

				}
			}
		}

		private readonly ServiceStackHost appHost;

		public ChallengeSetsServiceTest ()
		{
			try {
				appHost = new TestAppHost ();
				appHost.Init ();
			} catch (Exception e) {
				Assert.Fail ("Apphost startup failed: {0}", e.ToString ());
			}
		}

		[TestFixtureTearDown]
		public void TestFixtureTearDown ()
		{
			appHost.Dispose ();
		}

		[Test]
		public void FindChallengeSetsTest ()
		{
			using (var service = new ChallengeSetsService { Request = new MockHttpRequest () }) {
				var challengeSetIds = service.Get (new FindChallengeSets ());
				Assert.That (challengeSetIds.Count, Is.EqualTo (2));
				Assert.That (challengeSetIds.Contains (1));
				Assert.That (challengeSetIds.Contains (2));
			}
		}

		[Test]
		public void FindChallengeSetsWithFilterTest ()
		{
			using (var service = new ChallengeSetsService { Request = new MockHttpRequest () }) {
				var challengeSetIds = service.Get (new FindChallengeSets (){ ChallengeId = 1 });
				Assert.That (challengeSetIds.Count, Is.EqualTo (1));
				Assert.That (challengeSetIds.Contains (1));
				challengeSetIds = service.Get (new FindChallengeSets (){ ChallengeId = 42 });
				Assert.That (challengeSetIds.Count, Is.EqualTo (2));
				Assert.That (challengeSetIds.Contains (1));
				Assert.That (challengeSetIds.Contains (2));
			}
		}

		[Test]
		public void FindChallengeSetsWithBadFilterTest ()
		{
			using (var service = new ChallengeSetsService { Request = new MockHttpRequest () }) {
				var challengeSetIds = service.Get (new FindChallengeSets (){ ChallengeId = 666 });
				Assert.That (challengeSetIds.Count, Is.EqualTo (0));
			}
		}

		[Test]
		public void GetChallengeSetTest ()
		{
			var expected = new ChallengeSet {
				Id = 1,
				Title = "foobar",
				Description = "Foo bar foo bar.",
				Challenges = new List<int> () { 1, 2, 42 },
			};
			using (var service = new ChallengeSetsService { Request = new MockHttpRequest () }) {
				var challengeSet = service.Get (new GetChallengeSet (){ Id = 1 });
				Assert.That (challengeSet.Id, Is.EqualTo (expected.Id));
				Assert.That (challengeSet.Title, Is.EqualTo (expected.Title));
				Assert.That (challengeSet.Description, Is.EqualTo (expected.Description));
				Assert.That (expected.Challenges.SequenceEqual (challengeSet.Challenges));
			}
		}

		[Test]
		public void GetChallengeSetNonExistingTest ()
		{
			using (var service = new ChallengeSetsService { Request = new MockHttpRequest () }) {
				try {
					service.Get (new GetChallengeSet (){ Id = 666 });
					Assert.Fail ("Should throw on lookup up non existing ID");
				} catch (HttpError e) {
					Assert.That (e.StatusCode, Is.EqualTo (System.Net.HttpStatusCode.NotFound));
				}
			}
		}
	}
}

