﻿//
//  IPoints.cs
//
//  Author:
//       Mathias Tausig <mathias.tausig@fh-campuswien.ac.at>
//
//  Copyright (c) 2016 University of Applied Sciences Campus Vienna
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using ServiceStack;
using System.Collections.Generic;

namespace Hackathon.ServiceInterface.Points
{
	public interface IPoints
	{
		/// <summary>
		/// Gets the points a user gets awarded for a challenge
		/// </summary>
		/// <returns>The points.</returns>
		/// <param name="service">Service.</param>
		/// <param name="userId">User identifier.</param>
		/// <param name="challengeId">Challenge identifier.</param>
		int GetPoints (Service service, int userId, int challengeId);

		/// <summary>
		/// Gets the points a user gets awarded for a challenge.
		/// By letting you define the time the challenge was solved, this can be used to test the point value.
		/// </summary>
		/// <returns>The points.</returns>
		/// <param name="service">Service.</param>
		/// <param name="userId">User identifier.</param>
		/// <param name="challengeId">Challenge identifier.</param>
		/// <param name="solvedDate">The date when the challenge was solved. Set to DateTime.MinValue for an unsolved challenge.</param>
		/// <param name="accessDate">The date when the challenge was accessed.</param>
		/// <param name="accessedHintIds">A list of hints which were accessed.</param>
		int GetPoints (Service service, int userId, int challengeId, DateTime solvedDate, DateTime accessDate, IEnumerable<int> accessedHintIds);
	}
}

